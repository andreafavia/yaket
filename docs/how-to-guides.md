
## Binary classification 

You are developing a simple binary-classification model with standard Tensorflow optimizer, loss, and metrics.
We use binary-cross entropy, Adam optimizer, and a simple Model checkpoint callback that monitors validation loss.
The lists of available modules can be accesses using `Trainer.list_available_tf_modules(options)`. 
First, create your YAML file and save it in your working directory:

```yaml
    optimizer: Adam 
    loss: BinaryCrossentropy 
    batch_size: 128 
    callbacks:
        - ModelCheckpoint:
            monitor: val_loss
            mode: min
            verbose: 1
            save_best_only: True
            save_weights_only: True
            filepath: '/tmp/checkpoint'
    epochs: 100
```

We can then import the Trainer class from YAKET and train our model after loading training and validation datasets. In this case we have the datasets as numpy arrays, but YAKET supports also [tf.Data.Datasets](https://www.tensorflow.org/api_docs/python/tf/data/Dataset).


```python
    from yaket.trainer import Trainer

    path  = ... # path to the saved YAML file
    model = ... # tf.keras model

    trainer = Trainer(
        config_path=path,
        train_dataset=(x_train, y_train),
        val_dataset=(x_test, y_test),
        model=model, 
    )

    history = trainer.train()
```




## Multi-classification on GPUs 

YAKET allows training model on multiple GPUs using [tf.distribute](https://www.tensorflow.org/api_docs/python/tf/distribute) strategy. It uses by default one GPU if available or CPUs.
The only thing you'll need to do is to add the **accelarator** parameter equal to **mgpu**. If there's no GPUs, an error will be thrown at initialization time.
We can also add the accuracy metric and an Early stopping technique.

```yaml
    optimizer: Adam 
    loss: CategoricalCrossentropy 
    metrics:
    - categorical_accuracy
    batch_size: 128 
    callbacks:
        - ModelCheckpoint:
            monitor: val_loss
            mode: min
            verbose: 1
            save_best_only: True
            save_weights_only: True
            filepath: '/tmp/checkpoint'
        - EarlyStopping:
            monitor: val_loss
            mode: min
            patience: 10
    epochs: 100
    accelerator: mgpu
```


## Adding custom loss, optimizer, or metrics

Often you might need to try out new approaches that are not still available in Tensorflow.
Imagine we want to try using [CTCLoss](https://distill.pub/2017/ctc/) for speech recognition. 
We first define our loss in a Python script saved in our working directory:

```python
def CTCLoss(y_true, y_pred):
    # Compute the training-time loss value
    batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
    input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
    label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

    input_length = input_length * tf.ones(shape=(batch_len, 1), dtype="int64")
    label_length = label_length * tf.ones(shape=(batch_len, 1), dtype="int64")

    loss = keras.backend.ctc_batch_cost(y_true, y_pred, input_length, label_length)
    return loss
```


Then, we can directly use the name of the function as argument value in our YAML file:


```yaml
    optimizer:
        Adam:
            learning_rate: 0.01
    batch_size: 128 
    loss: CTCLoss 
    callbacks:
        - EarlyStopping
        - ReduceLROnPlateau:
            monitor: val_loss
            mode: min
            patience: 5
    verbose: 1
    epochs: 1
```

We then just need to call our Trainer class by passing the path of the file in which we created the custom loss and use it as before.
YAKET will automatically import that function and use it during training.

```python
    from yaket.trainer import Trainer

    path  = ... # path to the saved YAML file
    model = ... # tf.keras model
    custom_module_path = ... # path to Python script with custom Loss

    trainer = Trainer(
        config_path=path,
        train_dataset=(x_train, y_train),
        val_dataset=(x_test, y_test),
        model=model, 

        custom_modules_path=custom_module_path
    
    # you can override the number of epochs, useful during debugging
    trainer.train(epochs = 10) 
    )
```

## Monitoring with MLflow Autologging

Logging model's training process is important, especially during hyperparameter tuning and for data and model versioning.

YAKET supports [MLFlow autolog](https://www.mlflow.org/docs/latest/tracking.html#tensorflow-and-keras), hence all model's artifacts will be stored. 
MLFlow is integrated within [AzureML](https://docs.microsoft.com/en-us/azure/machine-learning/v1/how-to-use-mlflow?tabs=azuremlsdk), hence when submitting jobs using YAKET the training parameters, Tensorboard artefacts, and models will be automatically logged.

You just need to add one line of code in the YAML configuration file:

```yaml
    autolog: True # activate MLflow autolog
    ...

```
Below an example of the integration between MLFlow and AzureML:

![Metrics tracked on AzureML](imgs/Screenshot%202022-09-14%20160950.png)
**Figure 1** Metrics tracked automatically via Mlflow on AzureML GPU machine.

## Convert to ONNX or TensorFlow Lite

Once the model has been trainer, you might want to use optimized framework for deployment, especially for production.
[ONNX](https://onnx.ai/) and [TensorFlow Lite](https://www.tensorflow.org/lite) are two possible format used in real-life production environment. 


Converting a model to one of the format is as easy as writing a line of code.

```python
    ...
    
    trainer.train() 

    trainer.convert_model(format_model = 'onnx')
```

The command will convert the trained model to ONNX format and save it in the current working directory as `model.onnx`.

If you have a previously trained model, you can simply import the `Converter` class and use it in the following way:

```python
    from yaket.converter.converter import Converter
    model = ... # tf.keras.Model
    out_path =  ... # where to save the model
    out_format = ... # onnx or tflite

    converter = Converter(model = model, out_path = out_path, out_format = out_format)


    converter.convert()
```

**n.b.:** At the moment only the simple default configuration for converting models is available

## Override Trainer module for extra flexibility

Although YAKET covers many possible scenarios, you might need to implement some extra function that are not being taken care of (YET).
The easy-to-read code allows to quickly use Inheritance to create a custom trainer with modified or added modules.

At the momement, YAKET supports either custom optimizers defined in provided Python script or standard optimizers in tf.Keras.
You might want to use **AdamW** optimizer available in [TensorFlow Addons](https://www.tensorflow.org/addons). 

This is pretty easy with YAKET. You can just modify the [`_get_optimizer(self)`](https://gitlab.com/andreafavia/yaket/-/blob/main/yaket/trainer.py#L424) function by replacing `tensorflow.keras.optimizer` with `tensorflow_addons.optimizers`.


