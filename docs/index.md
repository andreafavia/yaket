This site contains the project documentation for the
`YAKET` project.

## Table Of Contents
1. [Explanation](explanation.md)
2. [How-To Guides](how-to-guides.md)
3. [Reference](reference.md)
