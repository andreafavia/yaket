## WHY
YAKET is born at [Sleepiz AG](https://www.sleepiz.com) as part of a wider framework for deep learning model development. 
Model development can be divided into three main steps:

1. **Data loader**: it is responsible for the generation of the input dataset, including augmentation and features extraction

2. **Model training + Architecture search**: the actual training of the model (or hyperparameter tuning of models) based on the predefined dataset

3. **Model evaluation**: the evaluation of the model based on predefined optimizing and satisfying metrics for a specific task.

**YAKET** addresses the second point by providing a Trainer class that can be used with Tensorflow Keras models, by defining training parameters directly in a human-readable format. 


## WHAT

It provides APIs for many standard use cases, and it support parameters configuration via YAML file, distributed training on GPUs, as well as an easy way to convert trained models to ONNX or TFLite types.


## HOW

The usage of [Trainer class](reference.md) is simple as passing training datasets and path to the configuration file and calling one single method:

```python
    
    model = ... # define your tf.keras.Model

    # Define path to yaml file
    path = "/yaket/examples/files/trainer.yaml"

    # Initialize trainer
    trainer = Trainer(
        config_path=path,
        train_dataset=(x_train, y_train),
        val_dataset=(x_test, y_test),
        model=model, 
    )
    # train based on the parameters defined in the yaml file
    history = trainer.train() 

```
